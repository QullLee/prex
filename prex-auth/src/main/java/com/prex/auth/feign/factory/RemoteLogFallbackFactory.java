package com.prex.auth.feign.factory;

import com.prex.auth.feign.RemoteUserService;
import com.prex.auth.feign.fallback.RemoteLogFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @Classname RemoteLogFallbackFactory
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-08-24 10:55
 * @Version 1.0
 */
@Component
public class RemoteLogFallbackFactory implements FallbackFactory<RemoteUserService> {
    @Override
    public RemoteUserService create(Throwable throwable) {
        return new RemoteLogFallbackImpl(throwable);
    }
}
