package com.prex.client.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.prex.client.service.IClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @ClassName ClientService
 * @Description TODO
 * @Author yanlin
 * @Version v1.0
 * @Date 2019-08-15 4:59 PM
 **/
@Slf4j
@Service
public class ClientService implements IClientService {
    @SentinelResource(value = "doSomeThing", blockHandler = "exceptionHandler")
    @Override
    public void clientTest(String str) {
        log.info(str);
    }
    // 限流与阻塞处理
    public void exceptionHandler(String str, BlockException ex) {
        //TODO 用户自行手动抛出异常，前端自行处理，例如429
        log.error( "blockHandler：" + str, ex);
    }
}
